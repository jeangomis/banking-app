package bankingApp.gomis.repository;

import bankingApp.gomis.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
