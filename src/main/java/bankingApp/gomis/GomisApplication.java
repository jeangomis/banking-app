package bankingApp.gomis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GomisApplication {

	public static void main(String[] args) {
		SpringApplication.run(GomisApplication.class, args);
	}

}
