package bankingApp.gomis.service;

import bankingApp.gomis.dto.AccountDto;
import bankingApp.gomis.entity.Account;

import java.util.List;

public interface AccountService {
    AccountDto createAccount(AccountDto accountDto);
    AccountDto getAccountById(Long id);
    AccountDto deposit(Long id, double amount);
    AccountDto withdraw(Long id, double amount);
    List<AccountDto> getAllAccounts();
    void  delelteAccount(Long id);
}
